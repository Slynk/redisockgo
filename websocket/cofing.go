package websocket

import (
	"time"
)

type Config struct {
	ReadChannels         []interface{}
	WriteChannels        []interface{}
	DuplacateHashTimeout time.Duration
	WriteTimeout         time.Duration
	PongTimeout          time.Duration
	PingPeriod           time.Duration
	MaxMessageSize       int64
}
