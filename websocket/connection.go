package websocket

import (
	"bitbucket.org/Slynk/redisockgo/request"
	"github.com/fzzy/radix/extra/pubsub"
	"github.com/fzzy/radix/redis"
	"github.com/gorilla/websocket"
	"github.com/thinkslynk/ttlcache"
	"log"
	"time"
)

type Connection struct {
	WS           *websocket.Conn
	RedisPub     *redis.Client
	RedisSub     *pubsub.SubClient
	Translator   request.Translator
	ErrorHandler func(error)
	Config       *Config
	WriteClosed  chan bool
	ReadClosed   chan bool
	RedisReceive chan *pubsub.SubReply
	HashCache    *ttlcache.Cache
}

// write writes a message with the given message type and payload.
func (c *Connection) write(mt int, payload []byte) error {
	c.WS.SetWriteDeadline(time.Now().Add(c.Config.WriteTimeout))
	return c.WS.WriteMessage(mt, payload)
}

func ReadPump(c *Connection) {
	defer func() {
		log.Println("Closing readpump go routine")
	}()
	if len(c.Config.WriteChannels) < 1 {
		return
	}

	defer func() {
		c.WS.Close()
		c.ReadClosed <- true
	}()

	c.WS.SetReadLimit(c.Config.MaxMessageSize)
	c.WS.SetReadDeadline(time.Now().Add(c.Config.PongTimeout))
	c.WS.SetPongHandler(func(string) error { c.WS.SetReadDeadline(time.Now().Add(c.Config.PingPeriod)); return nil })
	for {
		select {
		case <-c.WriteClosed:
			log.Println("Shutdown")
			return
		default:
			log.Println("Default")
			if messageType, message, err := c.WS.ReadMessage(); err != nil {
				if c.ErrorHandler != nil {
					log.Println("Handle error reading message")
					c.ErrorHandler(err)
				}
				return
			} else {
				request := c.Translator.InterpretIncoming(messageType, message)
				if request == nil {
					// TODO ask if should drop connection upon bad request or just ignore
				} else {
					log.Println("PUBLISH " + request.Channel + " " + request.Message)
					c.RedisPub.Cmd("PUBLISH", request.Channel, request.Message)
				}
			}
		}
	}
}

func WritePump(c *Connection) {
	ticker := time.NewTicker(c.Config.PingPeriod)
	defer func() {
		log.Println("Closing write pump")
		ticker.Stop()
		c.WS.Close()
		c.WriteClosed <- true
		c.WriteClosed <- true

	}()

	log.Println("Starting receive go routine")
	go func() {
		for {
			select {
			case <-c.WriteClosed:
				log.Println("Closing receive go routine")
				return
			default:
				reply := c.RedisSub.Receive()
				if !reply.Timeout() {
					c.RedisReceive <- reply
				}
			}

		}
	}()

	//log.Println("Before write pump loop: ", runtime.NumGoroutine())
	for {
		select {
		case <-c.ReadClosed:
			log.Println("Shutdown")
			return
		case reply := <-c.RedisReceive:
			if !reply.Timeout() && reply.Err == nil {
				log.Println(*reply)
				out := c.Translator.TranslateOutgoing(&request.Request{
					Channel: reply.Channel,
					Message: reply.Message,
				})

				if out != nil {
					if cached, found := c.HashCache.Get(out.Hash); !found || out.UID != cached {
						c.HashCache.Set(out.Hash, out.UID)
						if err := c.write(websocket.TextMessage, out.Message); err != nil {
							return
						}
					}
				}
			}
		case <-ticker.C:
			if err := c.write(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}
