package server

import (
	"bitbucket.org/Slynk/redisockgo/request"
	"bitbucket.org/Slynk/redisockgo/websocket"
	"github.com/fzzy/radix/extra/pool"
	"github.com/fzzy/radix/extra/pubsub"
	"github.com/fzzy/radix/redis"
	gorilla "github.com/gorilla/websocket"
	"github.com/thinkslynk/ttlcache"
	"log"
	"net/http"
	"time"
)

type Service struct {
	RedisProtocol       string
	RedisAddress        string
	RedisTimeout        time.Duration
	MaxRedisConnections int
	Upgrader            *gorilla.Upgrader
	Translator          request.Translator
	Authorize           func(http.ResponseWriter, *http.Request) *websocket.Config
	ErrorHandler        func(error)

	redisPool *pool.Pool
}

func (service *Service) Init() error {
	defer service.Shutdown()

	log.Println("Creating Redis pool...")

	// Open connection to redis service
	p, err := pool.NewCustomPool(service.RedisProtocol, service.RedisAddress, service.MaxRedisConnections, func(network, addr string) (*redis.Client, error) {
		client, err := redis.DialTimeout(network, addr, service.RedisTimeout)
		if err != nil {
			return nil, err
		}

		return client, nil
	})

	if err != nil {
		log.Println("FAILED!")
		return err
	}

	service.redisPool = p

	return nil

	// go func() {
	// 	for {
	// 		time.Sleep(5 * time.Second)
	// 		log.Println("Current go routines: ", runtime.NumGoroutine())
	// 	}
	// }()
}

func (service *Service) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	log.Println("New request!")
	// Authorize connection
	config := service.Authorize(w, r)
	if config == nil {
		return
	}

	// Upgrade connection to a websocket
	ws, err := service.Upgrader.Upgrade(w, r, nil)
	if err != nil {
		if service.ErrorHandler != nil {
			log.Println("Error upgrading connection")
			service.ErrorHandler(err)
		}
		return
	}

	// Get a connection to the redis cluster
	readClient, err := service.redisPool.Get()
	if err != nil {
		if service.ErrorHandler != nil {
			log.Println("Error getting redis connection 1")
			service.ErrorHandler(err)
		}

		ws.Close()
		return
	}

	writeClient, err := service.redisPool.Get()
	if err != nil {
		if service.ErrorHandler != nil {
			log.Println("Error getting reds connection 2")
			service.ErrorHandler(err)
		}

		readClient.Close()
		ws.Close()
		return
	}

	subclient := pubsub.NewSubClient(readClient)
	subclient.PSubscribe(config.ReadChannels...)

	conn := &websocket.Connection{
		WS:           ws,
		RedisPub:     writeClient,
		RedisSub:     subclient,
		Translator:   service.Translator,
		ErrorHandler: service.ErrorHandler,
		Config:       config,
		WriteClosed:  make(chan bool, 2),
		ReadClosed:   make(chan bool, 1),
		RedisReceive: make(chan *pubsub.SubReply, 1),
		HashCache:    ttlcache.NewCache(config.DuplacateHashTimeout),
	}

	defer subclient.PUnsubscribe()
	defer service.redisPool.Put(readClient)
	defer service.redisPool.Put(writeClient)
	defer conn.HashCache.Close()

	log.Println("Starting readpump go routine")
	go websocket.ReadPump(conn)
	websocket.WritePump(conn)
}

func (service *Service) Shutdown() {
	if service.redisPool != nil {
		service.redisPool.Empty()
	}
}
