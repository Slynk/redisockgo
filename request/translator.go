package request

type Translator interface {
	InterpretIncoming(messageType int, message []byte) *Request
	TranslateOutgoing(request *Request) *OutgoingRequest
}
