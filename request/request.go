package request

type Request struct {
	Channel string
	Message string
}

type OutgoingRequest struct {
	Message []byte
	Hash    string
	UID     string
}
